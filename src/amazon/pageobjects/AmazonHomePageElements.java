package amazon.pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AmazonHomePageElements {
	
	private WebElement element = null;
	private List<WebElement> elements = new ArrayList<WebElement>();
	
	public WebElement searchBar(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//input[@id='twotabsearchtextbox']"));
		
		return element;
	}
	
	public WebElement searchButton(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//input[@value='Go']"));
		
		return element;
	}
	
	public WebElement plasticcheckbox(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//input[@name='s-ref-checkbox-8080061011']"));
		
		return element;
	}
	
	public WebElement minTextField(WebDriver driver)
	{
		element = driver.findElement(By.id("low-price"));
		
		return element;
	}
	
	public WebElement maxTextField(WebDriver driver)
	{
		element = driver.findElement(By.id("high-price"));
		
		return element;
	}
	
	public WebElement goByRankButton(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//li[6]//span[contains(text(),'Go')]/preceding-sibling::input"));
		
		return element;
	}
	
	public List<WebElement> firstFiveResults(WebDriver driver)
	{
		elements = driver.findElements(By.xpath("//ul[@id='s-results-list-atf']/li[position()<6]"));
		
		return elements;
	}
	

}
